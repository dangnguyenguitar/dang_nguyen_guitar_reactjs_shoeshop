import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoes } from "./DataShoes";
import DetailShoe from "./DetailShoe";
import Header from "./Header";
import ProductList from "./ProductList";
export default class ShoeShop extends Component {
  state = {
    shoeArr: dataShoes,
    detail: dataShoes[0],
    cart: [],
  };
  handleViewDetail = (value) => {
    this.setState({
      detail: value,
    });
  };
  handleAddToCart = (shoe) => {
    // let cartItem = { ...shoe, number: 1 };
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container">
        <Cart cart={this.state.cart} />
        <Header />
        <div className="row mx-auto">
          <ProductList
            handleAddToCart={this.handleAddToCart}
            handleViewDetail={this.handleViewDetail}
            shoeArr={this.state.shoeArr}
          />
        </div>
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
