import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td className="align-middle">{item.id}</td>
          <td className="align-middle">{item.name}</td>
          <td className="align-middle">${item.price * item.number}</td>
          <td className="align-middle">{item.number}</td>
          <td className="align-middle">
            <img style={{ width: "100px" }} src={item.image} alt="" />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
