import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  renderProductList = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ProductItem
          handleAtC={this.props.handleAddToCart}
          handleView={this.props.handleViewDetail}
          data={item}
        />
      );
    });
  };

  render() {
    return this.renderProductList();
  }
}
