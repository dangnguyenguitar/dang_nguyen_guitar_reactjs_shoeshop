import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, price, description, image, quantity } = this.props.detail;
    return (
      <div
        className="modal fade"
        id="exampleModalCenter"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div
          className="modal-dialog modal-xl modal-dialog-centered"
          role="document"
        >
          <div className="modal-content p-4">
            <div className="modal-footer border-0 p-0">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                <i class="fa fa-times"></i>
              </button>
            </div>
            <div style={{ height: "400px" }} className="row">
              <img
                style={{ width: "100%", height: "100%", objectFit: "cover" }}
                src={image}
                alt=""
                className="col-6"
              />
              <div className="col-6 text-left pt-5">
                <h2>{name}</h2>
                <hr />
                <p>Price: ${price}</p>
                <hr />
                <p>Description: {description}</p>
                <hr />
                <p>In stock: {quantity} pairs left </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
