import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { name, price, shortDescription, image } = this.props.data;
    return (
      <div className="col-3 p-2">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{shortDescription}</p>
            <p className="card-text">Price: ${price}</p>
            <button
              onClick={() => {
                this.props.handleAtC(this.props.data);
              }}
              className="btn btn-dark"
            >
              Add to card <i class="fa fa-cart-plus"></i>
            </button>
            <button
              onClick={() => {
                this.props.handleView(this.props.data);
              }}
              className="btn btn-info ml-2"
              data-toggle="modal"
              data-target="#exampleModalCenter"
            >
              Details
            </button>
          </div>
        </div>
      </div>
    );
  }
}
